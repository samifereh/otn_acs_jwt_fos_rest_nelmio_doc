<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;

class ImportCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('products:import')
            // the short description shown while running "php bin/console list"
            ->setDescription('Import list of categories.')
            ->addArgument('file', InputArgument::REQUIRED, 'Your file name.')
            ->setHelp('This command allows you to export the list of products...')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $file = $input->getArgument('file');
        $basePath = $this->getContainer()->getParameter('kernel.project_dir');
        $target_dir = $basePath.'/web/uploads/';
        $target_file = $target_dir . $file;

        if (file_exists($target_file)){
            $articleService = $container->get('product.import.list');
            $articleService->ImportList($target_file);
            $output->writeln(sprintf('FINISHING LOADING file %s to DATABASE AT %s',$file,date('Y m d H:i:s')));
        }else{
            echo "Le fichier $file n'existe pas.";
        }
        return;
    }

}
