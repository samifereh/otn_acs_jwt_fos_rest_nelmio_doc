<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LOGSCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('article:import_logs')
            ->setDescription('Importer les articles non existants vers articleHistory')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $articleService = $this->getContainer()->get('add_article_history');
        $articleService->ImportArticleLogs();
        $output->writeln('Importation términé.');
    }

}
