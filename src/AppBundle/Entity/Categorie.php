<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie",
 * uniqueConstraints={@ORM\UniqueConstraint(name="designation",columns={"designation"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"detail", "list"})
     */
    private $id;

    public function __toString()
    {
        return $this->designation;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", unique=true, length=255)
     * @Serializer\Groups({"detail", "list"})
     */
    private $designation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Categorie
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }
}

