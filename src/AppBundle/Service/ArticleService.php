<?php
/**
 * Created by PhpStorm.
 * User: tritux
 * Date: 21/03/19
 * Time: 16:16
 */

namespace AppBundle\Service;

use AppBundle\Entity\Article;
use Doctrine\DBAL\Connection;
use AppBundle\Entity\ArticleHistory;
use AppBundle\Entity\Categorie;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class ArticleService
{

    protected $container;

    public function __construct(ContainerInterface $container) // this is @service_container
    {
        $this->container = $container;
    }

    public function addArticleHistory($data)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $timeStamp = date("Y-m-d H:i:s");
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $timeStamp);
        $idCategory = $data['categorie'];

        $repoCategory = $this->container->get('doctrine')->getRepository('AppBundle:Categorie');
        $categorie = $repoCategory->find($idCategory);

        /**
         * @var ArticleHistory $articleHistory
         */
        $articleHistory = new ArticleHistory();
        $articleHistory->setDesignation($data['designation']);
        $articleHistory->setCategorie($categorie);
        $articleHistory->setReference($data['reference']);
        $articleHistory->setDateentree($date);
        $articleHistory->setUpdatedAt(null);

        /**
         * @var EntityManager $em
         */
        $em->persist($articleHistory);
        $em->flush();
    }

    public function ImportList($file)
    {
        #$basePath = $this->container->getParameter('kernel.project_dir');
        #$uploadDirectory = $basePath.'/web/uploads/';
        #$filePath = $uploadDirectory.$file;

        $handle = fopen($file, "r");
        $stat = [
            "ok" => 0,
            "ko" => 0
        ];

        while(($fileop = fgetcsv($handle,1000, ";")) !== false)
        {
            $entityManager = $this->container->get('doctrine.orm.entity_manager');
            $categorie = new Categorie();
            $categorie->setDesignation($fileop[0]);
            $verif = $entityManager->getRepository('AppBundle:Categorie')
                ->findOneBy(['designation' => $fileop[0]]);
            if($verif){
                echo '"catégorie '.$verif.'"'.' déjà existante';
                echo "\n";
                $stat["ko"]++;
            }else{
                $entityManager->persist($categorie);
                $stat['ok']++;
                $entityManager->flush();
            }
        }
        return $stat;
    }

    public function ImportByLoadDataList($file)
    {
        #$basePath = $this->container->getParameter('kernel.project_dir');
        #$uploadDirectory = $basePath.'/web/uploads/';
        #$filePath = $uploadDirectory.$file;
        #$filePath = $uploadDirectory.$file;

        /**
         * @var Connection $connection
         */
        $connection = $this->container->get('doctrine')->getConnection();

        $command  = "LOAD DATA INFILE '$file' ";
        $command .= "IGNORE INTO TABLE categorie ";
        $command .= 'FIELDS TERMINATED BY "," ';
        $command .= "OPTIONALLY ENCLOSED BY '".'"'."' ";
        $command .= 'LINES TERMINATED BY "\n" ';
        $command .= '(designation);';

        $connection->prepare($command)->execute();
        return $file;
    }

    public function ImportArticleLogs(){
        $em = $this->container->get('doctrine')->getEntityManager();
        /* @var EntityManager $em */
        $articles= $em->getRepository('AppBundle:Article')->findAll();

        if(empty($articles)){
            return new JsonResponse(['message' => 'aucun article trouvé'], Response::HTTP_NOT_FOUND);
        }
        foreach($articles as $article){
            /* @var Article $article */
            $reference = $article->getReference();
            $exist= $em->getRepository('AppBundle:ArticleHistory')
                ->findOneBy(['reference' => $reference]);
            if($exist) continue;
            else{
                $articleHistory = new ArticleHistory();
                /* @var Article $article */
                $articleHistory->setDesignation($article->getDesignation());
                $articleHistory->setCategorie($article->getCategorie());
                $articleHistory->setReference($article->getReference());
                $articleHistory->setDateentree($article->getDateentree());
                $articleHistory->setUpdatedAt($article->getUpdatedAt());
                $em->persist($articleHistory);
                $em->flush();
            }
        }
    }

}