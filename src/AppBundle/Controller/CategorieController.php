<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Article;
use AppBundle\Entity\Categorie;
use AppBundle\Form\Type\CategorieType;


class CategorieController extends Controller
{

    /**
     * Lists all category entities.
     *
     * @Route("/categories", name="categories_list")
     * @Template("@App/categorie/index.html.twig")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:Categorie')->findAll();

        return ['categories' => $categories];
    }

    /**
     * Lists One category entity.
     *
     * @Route("categories/view/{id}", name="categorieDetail")
     * @Method("GET")
     * @Template("@App/categorie/show.html.twig")
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $categorie= $em->getRepository('AppBundle:Categorie')->find($id);
        #autre méthode si on cherche avec un autre paramètre
        #$repository = $this->getDoctrine()->getRepository('AppBundle:Categorie');
        #$categorie = $repository->findOneBy(['id'=> $id]);
        return ['categorie' => $categorie];
    }

    /**
     * Add a categorie entity.
     *
     * @Route("/categorie/new", name="add_categorie")
     * @Method("POST")
     */
    public function addAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $categorie = new Categorie();
        //on recupere le formulaire
        $form = $this->createForm(ParentsType::class, $categorie);
        //on génère le html du formulaire crée
        $formView = $form->createView();
        // Refill the fields in case the form is not valid.
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $donnee = $form->getData();
            $designation = $donnee->getDesignation();
            $categorie= $em->getRepository('AppBundle:Categorie')->findOneBy($designation);
            if ($categorie) {
                throw new NotFoundHttpException("La désignation ".$designation." existe déjà, veuillez choisir un autre.");
            }
            $em->persist($categorie);
            $em->flush();
            return $this->redirect($this->generateUrl('categories_list'));
        }
        //on rend la vue
        return $this->render('@App/categorie/index.html.twig',['form' => $formView]);
    }

    /**
     * Edit a categorie entity.
     *
     * @Route("/categorie/edit/{id}", name="edit_categorie")
     * @Method("PUT")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $categorie= $em->getRepository('AppBundle:Categorie')->find($id);

        if (!$categorie)
        {
            throw new NotFsoundHttpException("Aucune categorie trouvé");
        }

        //on recupere le formulaire
        $form = $this->createForm(ParentsType::class, $categorie);

        $em->flush();

        return $this->redirect($this->generateUrl('categories_list'));
    }

    /**
     * Delete a categorie entity.
     *
     * @Route("/categorie/delete/{id}", name="delete_categorie")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $categorie= $em->getRepository('AppBundle:Categorie')->find($id);

        if (!$categorie)
        {
            throw new NotFsoundHttpException("Aucune categorie trouvé");
        }

        $em->remove($categorie);
        $em->flush();

        return $this->redirect($this->generateUrl('categories_list'));
    }
    





}
