<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ArticleHistory;
use DateTime;
use DateTimeZone;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Article;
use AppBundle\Entity\Categorie;
use AppBundle\Form\Type\ArticleType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ArticleController extends Controller
{
    
    /**
     *@ApiDoc(
     *  resource=true,
     *  description="Liste des articles",
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Authorization Token key"
     *         }
     *     }
     * )
     * @Rest\View()
     * @Rest\Get("/api/articles")
     */
    public function getArticlesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AppBundle:Article')->findAll();

        $data = $this->get('jms_serializer')
            ->serialize($articles, 'json', SerializationContext::create()
            ->setGroups(array('list')));

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *@ApiDoc(
     *  resource=true,
     *  description="Détail de l'article",
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Authorization Token key"
     *         }
     *     }
     * )
     * @Rest\View()
     * @Rest\Get("/api/articles/{id}")
     */
    public function getArticleAction(Request $request, $id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        /* @var EntityManager $em */
        $article= $em->getRepository('AppBundle:Article')->find($id);

        $data = $this->get('jms_serializer')->serialize($article, 'json',
            SerializationContext::create()->setGroups(array('detail')));

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Ajouter des articles",
     *  requirements={
     *          {
     *              "name"="reference",
     *              "dataType"="string",
     *              "description"="Reference.",
     *              "required"="true"
     *          },
     *          {
     *              "name"="designation",
     *              "dataType"="string",
     *              "description"="Designation",
     *              "required"="true"
     *          },
     *          {
     *              "name"="categorie",
     *              "dataType"="integer",
     *              "description"="Categorie",
     *              "requirement"={"id"="\d+"},
     *              "required"="true"
     *          }
     *      },
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Authorization Token key"
     *         }
     *     }
     * )
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/api/articles")
     */
    public function postAction(Request $request)
    {
        $reference   = $request->get('reference');
        $em = $this->container->get('doctrine')->getEntityManager();
        $exist = $em->getRepository('AppBundle:Article')
            ->findOneBy(['reference' => $reference]);

        if(!$exist){

        $timeStamp = date("Y-m-d H:i:s");
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $timeStamp);

        $designation = $request->get('designation');
        $categorieId = $request->get('categorie');
        $categorie = $em->getRepository('AppBundle:Categorie')
            ->find($categorieId);

        
        $article = new Article();
        $article->setReference($reference);
        $article->setDesignation($designation);
        $article->setCategorie($categorie);
        $article->setDateentree($date);
        $article->setUpdatedAt(null);

        $em->persist($article);
        $em->flush();
        return $article;
        }else{
            return new JsonResponse(['Error' => 'Reference déjà existante !!'], Response::HTTP_OK);
        }

    }

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Ajouter des articles",
     *  requirements={
     *          {
     *              "name"="reference",
     *              "dataType"="string",
     *              "description"="Reference.",
     *              "required"="true"
     *          },
     *          {
     *              "name"="designation",
     *              "dataType"="string",
     *              "description"="Designation",
     *              "required"="true"
     *          },
     *          {
     *              "name"="categorie",
     *              "dataType"="integer",
     *              "description"="Categorie",
     *              "requirement"={"id"="\d+"},
     *              "required"="true"
     *          }
     *      },
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Authorization Token key"
     *         }
     *     }
     * )
     * @Rest\View()
     * @Rest\Put("/api/articles/{id}")
     */
    public function updateAction(Request $request)
    {
        return $this->updateArticle($request, true);
    }

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Ajouter des articles",
     *  requirements={
     *          {
     *              "name"="reference",
     *              "dataType"="string",
     *              "description"="Reference.",
     *              "required"="true"
     *          },
     *          {
     *              "name"="designation",
     *              "dataType"="string",
     *              "description"="Designation",
     *              "required"="true"
     *          },
     *          {
     *              "name"="categorie",
     *              "dataType"="integer",
     *              "description"="Categorie",
     *              "requirement"={"id"="\d+"},
     *              "required"="true"
     *          }
     *      },
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Authorization Token key"
     *         }
     *     }
     * )
     * @Rest\View()
     * @Rest\Patch("/api/articles/{id}")
     */
    public function patchAction(Request $request)
    {
        return $this->updateArticle($request, false);
    }

    public function updateArticle(Request $request, $clearMissing)
    {
        $id = $request->get('id');
        /** @var EntityManager $em **/
        $em = $this->container->get('doctrine')->getEntityManager();
        $article= $em->getRepository('AppBundle:Article')->find($id);

        if (!$article){
            return new JsonResponse(['message' => 'article not found'], Response::HTTP_NOT_FOUND);
        }

        $dateEntree = $article->getDateentree()->format('Y-m-d H:i:s');

        /** @var Article $article */
        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isSubmitted()){
            $timeStamp = date("Y-m-d H:i:s");
            $dateE = DateTime::createFromFormat('Y-m-d H:i:s', $dateEntree);
            $updateDate  = DateTime::createFromFormat('Y-m-d H:i:s', $timeStamp);
            /** @var Article $article */
            $article->setDateentree($dateE);
            $article->setUpdatedAt($updateDate);
            $article->setReference($request->request->get('reference'));
            #dump($article);die;
            $em->persist($article);
            $em->flush();
            return $article;
        }else{
            return $form;
        }
    }

    /**
     *@ApiDoc(
     *  resource=true,
     *  description="Delete article",
     *  requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="Id Article",
     *              "requirement"={"id"="\d+"},
     *              "required"="true"
     *          }
     *      },
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Authorization Token key"
     *         }
     *     }
     * )
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/api/articles/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        /**
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return $this->redirect($this->generateUrl('access_denied'));
        }
         **/

        /** @var EntityManager $em **/
        $em = $this->container->get('doctrine')->getEntityManager();
        $article= $em->getRepository('AppBundle:Article')->find($id);

        if (!$article) {
            return new JsonResponse(['message' => 'article not found'], Response::HTTP_NOT_FOUND);
        }
        $em->remove($article);
        $em->flush();
        return new JsonResponse(['OK' => 'article supprimé !!'], Response::HTTP_OK);
    }

    public function checkReferenceValidityAction(Request $request)
    {
        $reference = $request->get('reference');
        $em = $this->container->get('doctrine')->getEntityManager();
        $article= $em->getRepository('AppBundle:Article')->findOneBy(['reference' => $reference]);
        if($article){
            $response = new Response(
                'exist',
                Response::HTTP_OK,
                ['content-type' => 'text/plain']
            );
        }else{
            $response = new Response(
                'not_exist',
                Response::HTTP_OK,
                ['content-type' => 'text/plain']
            );
        }

        return $response;
    }

    public function AccessDeniedAction()
    {
        return $this->render('@App/exception/error403.html.twig');
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/articles/import")
     */
    public function importAction()
    {
        $basePath = $this->container->getParameter('kernel.project_dir');
        $target_dir = $basePath.'/web/uploads/';
        $uploadOk = 1;

        // Check if file is a actual csv file
        if(isset($_POST["submit"])) {
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            #check if csv file
            if($FileType != "csv"){
                $msg = "Sorry, only CSV files are allowed.";
                $uploadOk = 0;
            }elseif(file_exists($target_file)){
                $msg = "Sorry, file already exists.";
                $uploadOk = 0;
            }elseif($_FILES["fileToUpload"]["size"] > 500000){
                $msg = "Sorry, your file is too large.";
                $uploadOk = 0;
            }elseif($uploadOk == 0){
                $msg = "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            }else{
                move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
                chmod($target_file, 0777);
                $msg = "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                $uploadOk = 1;
            }

            if (file_exists($target_file)){
                $articleService = $this->container->get('product.import.list');
                $articleService->ImportList($target_file);
                return $this->redirect($this->generateUrl('articles_lists'));
            }
        }

        //on rend la vue
        return $this->render('@App/article/import.html.twig');
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/articles/logs")
     */
    public function logsArticleAction(Request $request)
    {
        $articleService = $this->container->get('add_article_history');
        $articleService->ImportArticleLogs();
        return new JsonResponse('Done',200);
    }

}
