<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation as Doc;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use App\CoreBundle\Entity\Article;
use App\ApiBundle\Representation\Articles;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SecurityController extends Controller
{
    
    /**
    * @Rest\Post("/login_check", name="login")
    * @Doc\ApiDoc(
    *     section="User",
    *     resource=true,
    *     description="Return properties token",
    *     requirements={
    *          {
    *              "name"="username",
    *              "dataType"="string",
    *              "description"="Username."
    *          },
    {
    *              "name"="password",
    *              "dataType"="string",
    *              "description"="Password"
    *          }
    *      },
    *      statusCodes={
    *         200="Success",
    *         401="Bad credentials",
    *     }
    * )
    *
    * @param Request $request
    *
    * @return Response
    */
   public function login_checkAction(Request $request)
   {
   }
}
