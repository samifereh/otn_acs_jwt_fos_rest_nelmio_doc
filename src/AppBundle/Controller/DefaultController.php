<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{

    /**
     * @Route("/hello", name="hello")
     */
    public function testAction()
    {
        return new Response("Hello World !");
    }

    /**
     * @Route("/bonjour/{name}", name="bonjour")
     * @Template("@App/default/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $name = $request->get('name');
        return ['name' => $name];
    }

    /**
     * @Route("/calcul/{a}/{b}", name="caclcul")
     * @Template("@App/default/calcul.html.twig")
     */
    public function calculAction(Request $request)
    {
        $a = $request->get('a');
        $b = $request->get('b');
        return ['a' => $a, 'b' => $b];
    }

    /**
     * @Route("/personne/infos/{id}/{nom}/{prenom}/{adresse}", name="infos")
     * @Template("@App/default/showPerson.html.twig")
     */
    public function showPersonAction(Request $request)
    {
        $id = $request->get('id');
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $adresse = $request->get('adresse');
        $date = date("d-m-y");  

        $personne = [
                        'id' => $id,
                        'nom' => $nom,
                        'prenom' => $prenom,
                        'adresse' => $adresse,
                        'date' => $date
                    ];
        
        return ['date' => $date, 'personne' => $personne];
    }

}
