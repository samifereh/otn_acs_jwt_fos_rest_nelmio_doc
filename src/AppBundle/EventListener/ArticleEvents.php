<?php

namespace AppBundle\EventListener;

// for Doctrine < 2.4: use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use AppBundle\Entity\Article;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class ArticleEvents
{

    protected $container;

    public function __construct(ContainerInterface $container) // this is @service_container
    {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entityManager = $args->getObjectManager();
        $entity = $args->getObject();

        if ($entity instanceof Article){
            /**
             * @var Article $entity
             */
            $entity->setUpdatedAt(new \Datetime());
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entityManager = $args->getObjectManager();
        $entity = $args->getObject();

        if ($entity instanceof Article){
        /**
         * @var Article $entity
         */
        $entity->setUpdatedAt(new \Datetime());
        }
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request   = $event->getRequest();

        $route = $event->getRequest()->attributes->get('_route');
        $method = $request->getMethod();

        if($route == 'add_article' && $method == 'POST'){
        $entity = $event->getRequest()->request->get('appbundle_article');
        $this->container->get('add_article_history')->addArticleHistory($entity);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response  = $event->getResponse();
        $request   = $event->getRequest();
        $kernel    = $event->getKernel();
        $container = $this->container;
        /**
        $route = $event->getRequest()->attributes->get('_route');
        $method = $request->getMethod();

        if($route == 'add_article' && $method == 'POST'){
            $entity = $event->getRequest()->request->get('appbundle_article');
            $this->container->get('add_article_history')->addArticleHistory($entity);
        }
        **/
    }

}