<?php

namespace UserBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\Extension\RuntimeExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;


class AppExtension  extends \Twig_Extension
{

    private $userservice;

    public function __construct($userservice=null)
    {
        $this->userservice = $userservice;
    }

    public function getFilters()
    {
        return [
            // the logic of this filter is now implemented in a different class
            new TwigFilter('civ' , array($this, 'sexeFilter')),
            new TwigFilter('role' , array($this, 'roleFilter')),
        ];
    }

	public function getFunctions(){
  	 	return [
      	'pseudo' => new \Twig_Function_Method($this, 'pseudo'),
        ];
	}

    //fonction utilisé dans le cas d'utilisation d'un filtre
    public function sexeFilter($user)
    {
        /**
         * @var User $user
         */
        $sexe = $user->getSexe();
        $fullName = $user->getLastname().' '.$user->getFirstname();
        $civ = ($sexe==0) ? 'Mr. '.$fullName : 'Mme/Mlle '.$fullName;
        return $civ;
    }
 
	public function pseudo($user){
	  	$pseudo = $this->userservice->getPseudoByUser($user);
		return $pseudo;
	}


    public function roleFilter($user)
    {
        /**
         * @var User $user
         */
        $UserRoles = $user->getRoles();
        #dump($UserRoles);die;
        if (in_array('ROLE_SUPER_ADMIN', $UserRoles))
             return 'SUPER ADMIN';
        else if (in_array('ROLE_ADMIN', $UserRoles))
             return 'ADMIN';
        else if (in_array('ROLE_MANAGER', $UserRoles))
             return'MANAGER';
        else return 'User';
    }
}