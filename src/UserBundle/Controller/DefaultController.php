<?php

namespace UserBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use UserBundle\Entity\User;
use UserBundle\Form\Type\UserType;


class DefaultController extends Controller
{

    /**
     * Lists all User entities.
     *
     * @Route("/users", name="users_list")
     * @Method("GET")
     * @Template("@User/Default/index.html.twig")
     */
    public function indexAction()
    {
        $users = $this->get('fos_user.user_manager')->findUsers();
        return ['users' => $users];
    }

    /**
     * @Route("/users/{id}", name="view_user")
     * @Method({"GET"})
     */
    public function ShowUserAction(Request $request, $id)
    {
        $serviceTrait = $this->get('app.service_trait');
        $user = $serviceTrait->getUserById($id);
        return $this->render('UserBundle:Default:show.html.twig', ['user' => $user]);
    }

    /**
     * Show User Detail
     *
     * @Route("/user/view/{id}", name="user_detail")
     * @Method("GET")
     * @Template("@User/Default/show.html.twig")
     */
    public function getUserAction(Request $request, $id)
    {
        $user = $this->get('fos_user.user_manager')->findUserBy(['id' => $id]);
        $mode = $request->get('mode');
        if (!$user) {
            throw new NotFoundHttpException("Aucun utilisateur trouvé");
        }

        return ['user' => $user, 'mode' => $mode];
    }

    /**
     * Register User
     *
     * @Route("/user/register", name="user_register")
     * @Method("POST")
     * @Template("@User/Default/new.html.twig")
     */
    public function registerAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $user = new User();
        //on recupere le formulaire
        $form = $this->createForm(UserType::class, $user);
        //on génère le html du formulaire crée
        $formView = $form->createView();
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $donnee = $form->getData();
            $user->setEnabled(true) ;

            /**
             * @var EntityManager $em
             */
            $em->persist($user);
            $em->flush();
            return $this->redirect($this->generateUrl('users_list'));
        }
        return ['form' => $formView];
    }

    /**
     * Update User
     *
     * @Route("/user/update/{id}", name="user_update")
     * @Method("POST")
     * @Template("@User/Default/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $this->get('fos_user.user_manager')->findUserBy(array('id' => $id));
        //on recupere le formulaire
        $form = $this->createForm(UserType::class, $user);
        //on génère le html du formulaire crée
        $formView = $form->createView();
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $donnee = $form->getData();

            dump($donnee);

            /**
             * @var User $user
             */
            $user->setEnabled(true) ;

            /**
             * @var EntityManager $em
             */

            $em->flush();
            return $this->redirect($this->generateUrl('users_list'));
        }
        return ['form' => $formView, 'user' => $user, 'mode' => 'edit'];
    }

     /**
     * Delete User
     *
     * @Route("/user/delete/{id}", name="delete_user")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $user = $this->get('fos_user.user_manager')->findUserBy(['id' => $id]);
        if(!$user){
            throw new NotFoundHttpException("Aucun utilisateur trouvé");
        }else{
            $this->get('fos_user.user_manager')->deleteUser($user);
            return $this->redirect($this->generateUrl('users_list'));
        }
    }

    /**
     * check Email Existance
     *
     * @Route("/user/check_email", name="check_validity")
     * @Method("POST")
     */
    public function checkEmailValidityAction(Request $request)
    {
        $email = $request->get('email');
        $username = $request->get('username');
        $existEmail = $this->get('fos_user.user_manager')->findUserBy(['email' => $email]);
        $existUsername = $this->get('fos_user.user_manager')->findUserBy(['username' => $username]);
        if($existEmail){
            $response = new Response('email taken');
        }elseif($existUsername){
            $response = new Response('username taken');
        }
        else{
            $response = new Response('not taken');}
        return $response;
    }

}