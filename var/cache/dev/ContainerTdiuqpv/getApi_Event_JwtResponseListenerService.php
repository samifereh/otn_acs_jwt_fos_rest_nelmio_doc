<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'api.event.jwt_response_listener' shared service.

include_once $this->targetDirs[3].'/src/AppBundle/EventListener/JWTResponseListener.php';

return $this->services['api.event.jwt_response_listener'] = new \AppBundle\EventListener\JWTResponseListener();
